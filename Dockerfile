FROM debian:stretch-slim

LABEL maintainer="tomascohen@theke.io"

# Valid: master, major.minor
ARG BRANCH

ENV PATH /usr/bin:/bin:/usr/sbin:/sbin
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update \
    && apt-get -y --allow-unauthenticated install \
      devscripts \
      pbuilder \
      dh-make \
      fakeroot \
      debian-archive-keyring \
      gnupg2 \
      build-essential \
      wget \
      git \
      libmodern-perl-perl \
   && rm -rf /var/cache/apt/archives/* \
   && rm -rf /var/lib/api/lists/*

RUN wget https://theke-space-one.nyc3.digitaloceanspaces.com/base_${BRANCH}.tgz \
        -O /base.tgz

VOLUME /koha
VOLUME /debs

ENV PERL5LIB=/koha

ADD build.sh /build.sh

CMD /build.sh
