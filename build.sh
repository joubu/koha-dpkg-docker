#!/bin/bash

cd /koha

mkdir -p /var/cache/pbuilder/
mv /base.tgz /var/cache/pbuilder/base.tgz

if [ -z "${VERSION}" ]; then
	VERSION_OPTIONS="--autoversion"
else
	VERSION_OPTIONS="-v ${VERSION} --noautoversion"
fi

./debian/build-git-snapshot \
	--basetgz base \
	-r /debs \
	${VERSION_OPTIONS} -d
